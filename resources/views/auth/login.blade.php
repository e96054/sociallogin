<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    @vite(['/resources/css/app.css', 'resources/js/app.js'])
    @vite(['/resources/sass/common.scss', 'resources/js/app.js'])
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kosugi+Maru&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/2c55636920.js" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="d-flex row justify-content-center">
            <div class="title_box">
                <div class="image_disposer"></div>
                <!-- <div class="login_circle"><h1 class="login_title">歌ってみた甲子園</h1></div> -->
            </div>
            <h2 class="user_noti">歌ってみた甲子園<br>投票ログイン</h2>
            <div class="provider_block">
                <div class="circle google">
                    <a href="{{ route('login.google') }}"><i class="fa-brands fa-google fa-lg icon_des"></i></a>
                </div>
                <div class="circle facebook">
                    <a href="{{ route('login.facebook') }}"><i class="fa-brands fa-facebook-f fa-lg icon_des"></i></i></a>
                </div>
                <div class="circle twitter">
                    <a href="{{ route('login.twitter') }}"><i class="fa-brands fa-twitter fa-lg icon_des"></i></a>
                </div>
                <!-- <a href="{{ route('login.github') }}" class="btn btn-dark btn-block mb-3">Login with Github</a> -->
            </div>
            <h3 class="dir">１日に１票投票できます。<br>投票期間 9/1 〜 9/31 </h3>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>



